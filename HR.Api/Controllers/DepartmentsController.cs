﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HR.DomainDriver;

namespace HR.Api.Controllers
{
    [EnableCors(origins: "http://localhost:13615", headers: "*", methods: "*")]
    public class DepartmentsController : ApiController
    {
        public Features.IDepartmentFeature<Data.HRContext> Provider = new DepartmentFeature<Data.HRContext>();
        // GET: api/Department
        public IEnumerable<Features.Types.DepartmentHeader> Get()
        {
            return Provider.Query()
                .Select(d=>new Features.Types.DepartmentHeader { AsEntity = d });
        }

        // GET: api/Department/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Department
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Department/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Department/5
        public void Delete(int id)
        {
        }
    }
}
