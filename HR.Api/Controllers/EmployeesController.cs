﻿using HR.DomainDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HR.Api.Controllers
{
    [EnableCors(origins: "http://localhost:13615", headers: "*", methods: "*")]
    public class EmployeesController : ApiController
    {
        public static Features.IEmployeeFeature<Data.HRContext> Provider = new EmployeeFeature<Data.HRContext>();
        // GET: api/Employees
        public IEnumerable<Features.Types.EmployeeHeader> Get(string employees, string departments = "")
        {
            return Provider.Query(new {
                employees=employees,
                departments=departments
            })
            .Select(e=>new Features.Types.EmployeeHeader { AsEntity = e });
        }

        // GET: api/Employees/5
        public Features.Types.EmployeeHeader Get(int id)
        {
            return Provider.Query()
                .Select(e=>new Features.Types.EmployeeHeader { AsEntity = e})
                .Single(e=>e.Id == id);
        }

        // POST: api/Employees
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Employees/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Employees/5
        public void Delete(int id)
        {
        }
    }
}
