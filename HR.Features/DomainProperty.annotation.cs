﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Features {
    public class DomainProperty : Attribute {
        public DomainProperty(PropertyInfo property) {
            Column = property.Name;
        }
        public DomainProperty(string property) {
            Column = property;
        }
        public readonly string Column;
    }
}
