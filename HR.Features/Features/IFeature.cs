﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.DomainModel;

namespace HR.Features
{
    public interface IFeature<T> where T : IContext, IDisposable, new ()
    {
        T Context { get; }
    }
}
