﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Features.Types {
    public class EmployeeHeader : TransactionalObject<Data.EMPLOYEE> {
        [DomainProperty("EMPLOYEE_ID")]
        public int Id { get; set; }

        [DomainProperty("EMAIL")]
        public string Email { get; set; }

        [DomainProperty("FIRST_NAME")]
        public string FirstName { get; set; }

        [DomainProperty("LAST_NAME")]
        public string LastName { get; set; }

        [DomainProperty("DEPARTMENT_ID")]
        public short DepartmentId { get; set; }
    }
}
