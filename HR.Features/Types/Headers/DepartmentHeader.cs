﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Features.Types {
    public class DepartmentHeader : TransactionalObject<Data.DEPARTMENT> {
        [DomainProperty("DEPARTMENT_ID")]
        public short Id { get; set; }

        [DomainProperty("DEPARTMENT_NAME")]
        public string Name { get; set; }

    }
}
