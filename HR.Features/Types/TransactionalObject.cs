﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace HR.Features.Types {
    public class TransactionalObject<EntityType> where EntityType : new () {
        protected EntityType Entity {get; set;}
        [IgnoreDataMember]
        public EntityType AsEntity {
            get {
                // Every public property of this class' (and decendants') instance except .AsEntity
                foreach(var property in this.GetType().GetProperties()) {
                    if (property.Name == "AsEntity") continue;
                    var possibleMappings = property.GetCustomAttributes(true); // 
                    bool mapped = false;
                    if (possibleMappings.Length>0) {
                        foreach (object attribute in possibleMappings) {
                            if (attribute is DomainProperty) {
                                this.Entity.GetType()
                                    .GetProperty(((DomainProperty) attribute).Column)
                                    .SetValue(this.AsEntity, property.GetValue(this));
                                mapped = true;
                                break;
                            }
                        }
                    }
                    if (!mapped) {
                        this.AsEntity.GetType()
                            .GetProperty(property.Name)
                            .SetValue(this.AsEntity, property.GetValue(this));
                    }
                }
                return this.Entity;
            }
            set {
                this.Entity = value;
                foreach (var property in this.GetType().GetProperties()) {
                    if (property.Name == "AsEntity") continue;
                    var possibleMappings = property.GetCustomAttributes(true);
                    bool mapped = false;
                    if (possibleMappings.Length>0) {
                        foreach (object attribute in possibleMappings) {
                            if (attribute is DomainProperty) {
                                this.GetType()
                                    .GetProperty(property.Name)
                                    .SetValue(this, Entity.GetType()
                                                        .GetProperty(((DomainProperty) attribute).Column)
                                                        .GetValue(this.Entity));
                                mapped = true;
                                break;
                            }
                        }
                    }
                    if (!mapped) {
                        this.GetType()
                            .GetProperty(property.Name)
                            .SetValue(this, value.GetType()
                                                .GetProperty(property.Name)
                                                .GetValue(this.Entity));
                    }
                }
            }
        }
    }
}
