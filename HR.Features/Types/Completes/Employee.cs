﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR.Features.Types {
    public class Employee : EmployeeHeader {
        public Employee():base() { }

        [DomainProperty("PHONE_NUMBER")]
        public string Phone { get; set; }

        [DomainProperty("EMPLOYEE1")]
        public EmployeeHeader ReportsTo { get; set; }
    }
}
