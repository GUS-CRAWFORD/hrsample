﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HR.DomainDriver;
using HR.Features;
using HR.DomainModel;

namespace HR.Features
{
    public abstract class Feature<T> : IFeature<T> where T : IContext, IDisposable, new ()
    {
        public T Context {
            get {
                return new T();
            }
        }
        public Feature () {

        }

    }
}
