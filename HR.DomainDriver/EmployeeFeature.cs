﻿using HR.Data;
using HR.Features;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HR.DomainDriver {
    public class EmployeeFeature<TContext> : Feature<TContext>, IEmployeeFeature<TContext> where TContext : HRContext, new() {
        public EmployeeFeature () : base () {

        }
        public IEnumerable<EMPLOYEE> Query () {
            using (var context = Context as Data.HRContext) {
                return context.EMPLOYEES;
            }
        }
        public IEnumerable<EMPLOYEE> Query (object filterTo) {
            Expression<Func<Data.EMPLOYEE, bool>> employeePredicate = null;
            if (filterTo == null) {
                employeePredicate = PredicateBuilder.True<Data.EMPLOYEE>();
            }
            else {
                employeePredicate = PredicateBuilder.False<Data.EMPLOYEE>();
                foreach (var property in filterTo.GetType().GetProperties()) {
                    if (((string)property.GetValue(filterTo)).Equals(String.Empty)) { continue; }
                    if (property.Name == "departments") {
                        foreach (var departmentId in ((string)property.GetValue(filterTo)).Split(',')) {
                            var shortDepartmentId = short.Parse(departmentId);
                            employeePredicate = employeePredicate
                                .Or(emp=>emp.DEPARTMENT_ID==shortDepartmentId);
                        }
                    }
                    else if (property.Name == "employees") {
                        foreach (var namePart in ((string)property.GetValue(filterTo)).Split(',')) {
                            employeePredicate = employeePredicate
                                .Or(emp=>emp.FIRST_NAME.Contains(namePart))
                                .Or(emp=>emp.LAST_NAME.Contains(namePart))
                                .Or(emp=>emp.EMAIL.Contains(namePart));
                        }
                    }
                }
            }
            using (var context = Context as Data.HRContext) {
                return context.EMPLOYEES.AsExpandable()
                    .Where(employeePredicate);
            }
        }
    }
}
