﻿(function (win, ng){
    'use strict';
    ng.module('app', ['ui.router', 'ui.bootstrap', 'ngRoute', 'ngResource', 'ngAnimate'])
        .constant('states', {
            home: {
                url:'/',
                controller: 'home.view',
                templateUrl: 'app/home/partials/home.html'
            },
            employee: {
                url: '/employee/:id',
                controller: 'employee.view',
                templateUrl: 'app/employee/partials/employee.html',
                resolve: {
                    employee: function (HR, $stateParams) {
                        return HR.Employee.get({ id: $stateParams.id }).$promise;
                    },
                    $uibModalInstance: function ($location) {
                        return {
                            close: function () {
                                $location.path('/');
                            }
                        }
                    }
                }
            },
            'edit': {
                url: '/employee/:id/edit',
                templateUrl: 'app/employee/partials/employee.html',
                resolve: {
                    employee: function (HR, $stateParams) {
                        return HR.Employee.get({ id: $stateParams.id }).$promise;
                    }
                },
                controller: ['$scope', 'employee', '$uibModal', '$state', 'hrTasks', function ($scope, employee, $uibModal, $state, hrTasks) {
                    $scope.employee = employee;
                    if (!hrTasks.pending.employees[employee.Id]) {
                        hrTasks.pending.employees.add(employee.Id, employee);
                        $uibModal.open({
                            controller: 'employee.edit.view',
                            templateUrl: 'app/employee/partials/employee.edit.html',
                            resolve: {
                                employee: employee
                            }
                        })
                        .result.then(function () {
                            $state.go('employee', { id: employee.Id });
                            hrTasks.pending.employees.remove(employee.Id, employee)
                        });
                    }

                }],
                resolve: {
                    employee: function (HR, $stateParams) {
                        return HR.Employee.get({ id: $stateParams.id }).$promise;
                    },
                    $uibModalInstance: function ($location) {
                        return {
                            close: function () {
                                $location.path('/');
                            }
                        }
                    }
                }
            }
        })
        .config(function ($stateProvider, states) {
            for (var state in states) {
                $stateProvider
                    .state(state, states[state]);
            }

        })
        .run(function ($state) {
            $state.go('home');
        });
})(window, angular);