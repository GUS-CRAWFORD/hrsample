﻿(function (win, ng) {
    'use strict';
    ng.module('app')
        .factory('hrTasks', hrTasks)
    hrTasks.$inject = ['HR'];
    function hrTasks(HR) {
        // [-] Private
        var employees = [];
        var departments = [];
        var editing = {
            employees: {
                get add() {
                    return function (key, employee) {
                        return editing.employees[key] = employee;
                    }
                },
                get remove() {
                    return function (key, employee) {
                        if (editing.employees[key]) {
                            delete editing.employees[key];
                            return employee;
                        }
                        else return employee;
                    }
                }
            }
        };
        var pending = {
            get employees() {
                return editing.employees;
            }
        };
        function overwriteAngular$res($res, overwritten) {
            $res.splice(0, $res.length);
            ng.forEach(overwritten, function ($item) {
                $res.push($item);
            });
            return $res;
        }
        // [+] Public Binds
        var hrTasks = {
            get employees() {
                return employees;
            },
            set employees(overwritten) {
                return overwriteAngular$res(employees, overwritten);
            },
            get departments() {
                return departments;
            },
            set departments(overwritten) {
                return overwriteAngular$res(departments, overwritten);
            },
            get pending() {
                return pending;
            }
        };

        // [>] Do once
        HR.Department.query().$promise
            .then(function (data) {
                hrTasks.departments = data;
            });

        return hrTasks;
    }
})(window, angular);