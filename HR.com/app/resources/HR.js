﻿(function (win, ng) {
    'use strict';
    ng.module('app')
        .constant('HR_REST',{
            URI: 'http://localhost:1576/api',
            EMPLOYEES: '/Employees/:id',
            DEPARTMENTS: '/Departments/:id'
        })
        .factory('HR', HRFactory);
    HRFactory.$inject = ['$resource','HR_REST'];
    function HRFactory($resource, HR_REST) {
        return {
            Employee: $resource(HR_REST.URI+HR_REST.EMPLOYEES),
            Department: $resource(HR_REST.URI+HR_REST.DEPARTMENTS)
        }
    }
})(window, angular);