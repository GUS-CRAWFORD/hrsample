﻿(function (ng) {
    'use strict';
    ng.module('app')
        .controller('employee.edit.view', employeeEditController);

    employeeEditController.$inject = ['$scope', 'hrTasks', 'employee', '$uibModalInstance'];
    function employeeEditController($scope, hrTasks, employee, $uibModalInstance) {
        $scope.employee = employee;
        $scope.edited = ng.copy(employee);
        $scope.close = $uibModalInstance.close;
    }
})(angular);