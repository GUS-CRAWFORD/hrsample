﻿(function (ng) {
    'use strict';
    ng.module('app')
        .controller('employee.view', employeeController);

    employeeController.$inject = ['$scope', 'hrTasks', 'employee', '$uibModalInstance', '$state', '$uibModal'];
    function employeeController($scope, hrTasks, employee, $uibModalInstance, $state, $uibModal) {
        $scope.employees = hrTasks.employees;
        $scope.employee = employee;
        $scope.close = $uibModalInstance.close;
        $scope.edit = function (employee) {
            if ($uibModalInstance.dismiss) $uibModalInstance.dismiss();
            $state.go('edit', { id: employee.Id });
        }

    }
})(angular);