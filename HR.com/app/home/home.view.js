﻿(function (ng) {
    'use strict';
    ng.module('app')
        .controller('home.view', homeController);

    homeController.$inject = ['$scope', 'hrTasks', '$uibModal','HR'];
    function homeController($scope, hrTasks, $uibModal, HR) {
        $scope.loading = true;
        $scope.departments = hrTasks.departments;
        HR.Department.query().$promise.then(function () { $scope.loading = false });
        $scope.preview = function (employee) {
            return $uibModal.open({
                controller: 'employee.view',
                templateUrl: 'app/employee/partials/employee.html',
                resolve: {
                    employee: employee
                }
            }).result;
        }

        $scope.expand = function (department) {
            department.expanded = !department.expanded;
            if (!department.employees || department.refresh) {
                $scope.loading = true;
                HR.Employee.query({ departments: department.Id })
                    .$promise.then(function (data) {
                        department.employees = data;
                        $scope.loading = false;
                    });
            }
        }
    }
})(angular);