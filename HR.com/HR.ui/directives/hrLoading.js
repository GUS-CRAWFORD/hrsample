﻿(function (ng) {
    'use strict';
    ng.module('app')
        .controller('hrLoading.view',hrLoadingController)
        .directive('hrLoading', hrLoading);
    hrLoading.$inject = [];
    function hrLoading () {
        return {
            restrict: 'C',
            scope: {
                activated: '='
            },
            templateUrl: 'HR.ui/directives/hrLoading.html',
            controller:'hrLoading.view'
        };
    }
    hrLoadingController.$inject = ['$scope'];
    function hrLoadingController ($scope) {

    }
})(angular)